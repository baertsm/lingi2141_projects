How to compile
==============

Simply launch `make` command in this directory. (It's possible to compile only
the client with `make client` and the server with `make server`).

How to use it
=============

First, launch the server with this command:

    ./server.bin PORT FILE

Then, launch the client with this command:

    ./client.bin ADDRESS PORT FILE

Info
====

Here is a reliable transmission protocol on top of UDP, using IPv6. This protocol is based on go-back-n.

The client will send a file to the server and thanks to go-back-n, the server will receive data in the right order.
