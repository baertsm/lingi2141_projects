#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>

#include <semaphore.h>
#include <pthread.h>

#include "../common/utils.h"
#include "file_mgr.h"
#include "net_mgr.h"
#include "exec.h"

#define TIMEOUT 5

int main(int ac, char **av)
{
	int exit_status;

	if (ac < 4) {
		fprintf(stderr, "Usage: %s <host> <port> <filepath>\n", av[0]);
		return 1;
	}

	if (open_file (av[3]) != 0)
		return EXIT_FAILURE;

	if (net_setup_socket (av[1], av[2]) != 0) {
		close_file ();
		return EXIT_FAILURE;
	}

	if (exec_init () != 0)
		exit_status = EXIT_FAILURE;

	else if (exec_send_file () != 0)
		exit_status = EXIT_FAILURE;

	else
		exit_status = EXIT_SUCCESS;

	exec_end ();

	return exit_status;

}
