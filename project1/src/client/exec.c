#include <stdlib.h> // malloc
#include <stdio.h>  // fprintf
#include <string.h> // strerror
#include <errno.h>  // errno
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <semaphore.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

#include "../common/utils.h"
#include "../common/xor.h"
#include "../common/udp.h"
#include "../common/packet.h"
#include "file_mgr.h"
#include "net_mgr.h"
#include "exec.h"

#define BUFF_ELEM 10 // SEQNUM_MAX_SIZE % BUFF_ELEM == 0
#define TIMEOUT 2

static uint8_t s_seqnum = 0;     // the current sequence number
static uint8_t s_seqnum_ack = 0; // the last ACK received
static uint8_t s_xor_freq = 0;   // the frequency of the XOR packages
static pthread_t waiting_threads[BUFF_ELEM];
static pthread_t s_receiver_thread;
static pthread_cond_t condition_stop[BUFF_ELEM];
static pthread_mutex_t s_mut_packet[BUFF_ELEM];
static pthread_mutex_t conditions_mut[BUFF_ELEM];
static sem_t s_sem_buff;

static packet_t **s_packets = NULL;
static pthread_mutex_t s_mutex_send;
static pthread_mutex_t s_mutex_retransmit;

static uint8_t FLAG_END = 0;

static void * waiting_thread (void* index);
static void * _receiver_thread (void* arg);

int exec_init (void)
{
	int i, err;
	uint8_t window;

	err = net_init_connexion (&s_xor_freq, &window);
	if (err != 0)
		return err;

	if (s_xor_freq > 0)
		s_xor_freq++; // if we receive 2, it means 2 data then 1 XOR

	if (BUFF_ELEM < s_xor_freq) {
		fprintf (stderr, "The XOR frequency (%d) is bigger than our buffer (%d)\n",
		         s_xor_freq, BUFF_ELEM);
		return -1;
	}

	s_packets = packets_init (BUFF_ELEM);
	if (s_packets == NULL)
		return -1;

	for (i = 0 ; i < BUFF_ELEM ; i++) {
		int *n = (int*) malloc(sizeof(int));
		*n = i;
		if (pthread_create(waiting_threads + i, NULL, waiting_thread, n) != 0 ) {
			fprintf (stderr, "Failed to create waiting thread: %s\n", strerror (errno));
		}
		if (pthread_mutex_init(conditions_mut + i, NULL) != 0) {
			fprintf (stderr, "Failed to init condition mutex: %s\n", strerror (errno));
		}
		if (pthread_mutex_init(s_mut_packet + i, NULL) != 0) {
			fprintf (stderr, "Failed to init condition: %s\n", strerror (errno));
		}
		if (pthread_cond_init(condition_stop + i, NULL) != 0) {
			fprintf (stderr, "Failed to init condition: %s\n", strerror (errno));
		}
	}

	if (pthread_mutex_init(&s_mutex_send, NULL) != 0) {
		fprintf (stderr, "Failed to init mutex: %s\n", strerror (errno));
		return errno;
	}
	if (pthread_mutex_init(&s_mutex_retransmit, NULL) != 0) {
		fprintf (stderr, "Failed to init mutex: %s\n", strerror (errno));
		return errno;
	}
	if (sem_init (&s_sem_buff, 0, BUFF_ELEM) != 0) { // send max BUFF_ELEM packets at the same time
		fprintf (stderr, "Failed to init semaphore: %s\n", strerror (errno));
		return errno;
	}
	if (pthread_create(&s_receiver_thread, NULL, _receiver_thread, NULL) != 0 ) {
		fprintf (stderr, "Failed to create receiver thread: %s\n", strerror (errno));
	}

	return 0;
}

static int _send_packet (packet_t *packet, int n)
{
	int err;

	if (pthread_mutex_lock(&s_mutex_send) != 0)
		return -1;

	
	err = net_send_packet (packet->type, packet->seq_num, packet->data, packet->len);

	if (pthread_mutex_unlock(&s_mut_packet[n]))
		return -1;

	if (pthread_mutex_unlock(&s_mutex_send) != 0)
		return -1;
	return err;
}

int exec_send_file (void)
{
	int n;
	uint16_t length;
	packet_t *packet;
	do {
		if (pthread_mutex_lock(&s_mutex_retransmit) != 0)
			return -1;
		s_seqnum = (s_seqnum + 1) % SEQNUM_MAX_SIZE ;
		n = (s_seqnum % BUFF_ELEM);
	
		packet = s_packets[n];
		packet->seq_num = s_seqnum;
		// XOR package
		if (s_xor_freq > 0 && (s_seqnum % s_xor_freq) == 0) {
			LOG ("XOR Package");
			xor_create_packet (s_packets, packet, s_xor_freq, n, BUFF_ELEM,
							   s_seqnum);
		}
		else {
			LOG ("Reading file");
			packet->type = PTYPE_DATA;
			if (read_file (packet->data, &length) != 0)
				return -1;
			packet->len = length;
		}
		
		if (sem_wait (&s_sem_buff) != 0)  {
			fprintf (stderr, "Failed to wait sem: %s\n", strerror (errno));
			return errno;
		}
		if (_send_packet (packet, n) != 0)
			return -1;

		packet->state = PACKET_SENT;
		if (pthread_mutex_unlock(&s_mutex_retransmit) != 0 ) {
			return -1;
		}
		LOG ("File chunk sent");
	} while (length > 0);
	
	printf ("All data have been sent\n");
	FLAG_END = 1;

	if (pthread_join(s_receiver_thread, NULL) != 0 ) { // wait for all ack
		return -1;
	}

	return 0;
}

static int _accept_packet (packet_t *packet, int n)
{
	int i, err = 0;

	packet->state = ACK_RECEIVED;
	pthread_cond_signal(condition_stop + n);
	if (packet->type == PTYPE_XOR) { // if 
		for	(i = 0 ; i < s_xor_freq ; i++) {
			s_packets[utils_get_index (BUFF_ELEM, n - i)]->state = EMPTY;
			sem_post (&s_sem_buff);
		}
	}

	return err;
}

/**
 * @brief Read ACK
 */
static void * _receiver_thread (void* arg)
{
	ptype_t type;
	uint8_t window;
	uint8_t seqnum = 0;
	uint8_t seqnum_expected = 0;
	int i, n, skip, retransmit;
	UNUSED(arg);

	printf ("Receiver thread launched!\n");

	while (FLAG_END == 0) {
		net_get_ack(&type, &window, &seqnum);

		if (type != PTYPE_ACK) {
			printf("Wrong type: expected ACK, received: %u\n", type);
			continue;
		}

		skip = 0; retransmit = 0;
		seqnum_expected = (s_seqnum_ack + 1) % SEQNUM_MAX_SIZE;
		if (s_seqnum_ack == seqnum) // the same as the previous one: skip
			printf ("Receive the same ACK as the previous one, skip it\n");
		else if (seqnum_expected == seqnum) // a new ack, the next one
			skip = 1;
		else if (seqnum_expected < seqnum) {
			if ((seqnum - seqnum_expected) > BUFF_ELEM) // /!\ SEQNUM_MAX_SIZE
				retransmit = SEQNUM_MAX_SIZE - (seqnum - seqnum_expected);
			else
				skip = seqnum - seqnum_expected;
		}
		else {
			if ((seqnum_expected - seqnum) > BUFF_ELEM) // /!\ SEQNUM_MAX_SIZE
				skip = SEQNUM_MAX_SIZE - (seqnum_expected - seqnum);
			else
				retransmit = seqnum_expected - seqnum;
		}

		n = (seqnum % BUFF_ELEM);
		s_seqnum_ack = seqnum;
		s_packets[n]->state = ACK_RECEIVED;

		if (retransmit > 0) {
			// printf ("We need to retransmit %d packets\n", retransmit);
			if (pthread_mutex_lock(&s_mutex_retransmit) != 0)
				fprintf (stderr, "Failed to lock waiting mutex: %s\n", strerror (errno));
			for (i = 0 ; i < retransmit ; i++) {
				if (_send_packet (s_packets[(n + i) % BUFF_ELEM], (n + i % BUFF_ELEM)) != 0)
					return NULL;
			}
			if (pthread_mutex_unlock(&s_mutex_retransmit) != 0)
				fprintf (stderr, "Failed to unlock waiting mutex: %s\n", strerror (errno));
		}
		else if (skip > 0) {
			// printf ("We accept %d ack in one time\n", skip);
			for (i = skip ; i > 0 ; i--) {
				if (_accept_packet (s_packets[utils_get_index (BUFF_ELEM, n - i)], n) != 0)
					return NULL;
			}
		}
	}
	return 0;
}


static void * waiting_thread (void* index)
{
	int n = * ((int*) index), result;
	free(index);
	struct timespec abstime;
	if (pthread_mutex_lock (s_mut_packet + n) != 0) {
		fprintf (stderr, "Failed to lock waiting mutex: %s\n", strerror (errno));
	}
	while (FLAG_END == 0) {
		if (pthread_mutex_lock (s_mut_packet + n) != 0) {
			fprintf (stderr, "Failed to lock waiting mutex: %s\n", strerror (errno));
		}
		if (FLAG_END == 1) // skip
			break;

		if ( pthread_mutex_lock(conditions_mut + n) != 0) {
			fprintf(stderr, "Failed to lock condition mutex: %s\n", strerror (errno));
		}
		current_utc_time(&abstime);
		abstime.tv_sec += TIMEOUT;
		result = pthread_cond_timedwait(condition_stop + n, conditions_mut + n, &abstime);
		if ( pthread_mutex_unlock(conditions_mut + n) != 0) {
			fprintf(stderr, "Failed to lock condition mutex: %s\n", strerror (errno));
		}
		if ( result == 0) {
			continue;
		}
		else { //time-out
			if (pthread_mutex_lock(&s_mutex_retransmit) != 0)
				fprintf (stderr, "Failed to lock waiting mutex: %s\n", strerror (errno));
			_send_packet(s_packets[n], n);
			if (pthread_mutex_unlock(&s_mutex_retransmit) != 0)
				fprintf (stderr, "Failed to unlock waiting mutex: %s\n", strerror (errno));
		}
	}
	
	return NULL;
}


int exec_end (void)
{
	int err, i;

	err = net_close_socket ();
	err |= close_file ();

	packets_free (s_packets, BUFF_ELEM);
	s_packets = NULL;

	if (pthread_mutex_destroy(&s_mutex_send) != 0) {
		fprintf (stderr, "Failed to destroy mutex: %s\n", strerror (errno));
		return errno;
	}
	if (pthread_mutex_destroy(&s_mutex_retransmit) != 0) {
		fprintf (stderr, "Failed to destroy mutex: %s\n", strerror (errno));
		return errno;
	}
	for (i = 0; i < BUFF_ELEM; i++) {
		pthread_mutex_destroy(conditions_mut + i);
		pthread_mutex_unlock(s_mut_packet + i); // unlock to end the thread
		pthread_mutex_destroy(s_mut_packet + i);
		pthread_cond_destroy(condition_stop + i);
	}
	if (sem_destroy (&s_sem_buff) != 0) {
		fprintf (stderr, "Failed to destroy semaphore: %s\n", strerror (errno));
		return errno;
	}

	return err;
}
