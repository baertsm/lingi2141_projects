#ifndef _EXEC_H_
#define _EXEC_H_

/**
 * @brief initialise the transfer with Go Back End support
 * @return 0 on success or errno
 */
int exec_init (void);

/**
 * @brief send the current file with Go Back End support
 * @return 0 on success or errno
 */
int exec_send_file (void);

/**
 * @brief end the transfer, close the socket and the file and free resources
 * @return 0 on success or errno
 */
int exec_end (void);

#endif
