#include <string.h> // stderror
#include <errno.h>  // errno
#include <sys/stat.h> // open
#include <fcntl.h>    // open
#include <unistd.h>   // close

#include "../common/utils.h"

#include "file_mgr.h"

static int s_filefd;

int open_file (const char* filename)
{
	s_filefd = open (filename, O_RDONLY);
	if (s_filefd < 0) {
		fprintf (stderr, "Failed to open the file to read: %s\n",
				 strerror (errno));
		return errno;
	}
	return 0;
}

int read_file (char buf[PACKET_SIZE], uint16_t *length)
{
	ssize_t bytes;

	bytes = read (s_filefd, &buf[PACKET_HEADER], PACKET_PAYLOAD);
	if (bytes == -1) {
		fprintf (stderr, "Failed to open the file to read: %s\n",
				 strerror (errno));
		return errno;
	}

	*length = bytes;

	if (bytes < PACKET_PAYLOAD) // add 0
		memset (&buf[PACKET_HEADER + bytes], 0, PACKET_PAYLOAD - bytes);

	return 0;
}

int close_file (void)
{
	int err;

	err = close (s_filefd);
	if (err == -1) {
		fprintf (stderr, "Failed to close the file: %s\n", strerror (errno));
		return errno;
	}

	return 0;
}
