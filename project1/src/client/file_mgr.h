#ifndef _FILE_MGR_H_
#define _FILE_MGR_H_

#include <stdint.h> // uint16_t
#include "../common/udp.h" // PACKET_SIZE

/**
 * @brief Open the file with read rights
 * @param filename the name of the file
 * @return 0 on success or errno
 */
int open_file (const char* filename);

/**
 * @brief read the current file and filled the buffer in
 * @param buf    the buffer to fill in
 * @param length the length of data read (max PACKET_PAYLOAD)
 * @return 0 on success or errno
 */
int read_file (char buf[PACKET_SIZE], uint16_t *length);

/**
 * @brief Close the opened file
 * @return 0 on success or errno
 */
int close_file (void);

#endif
