#include <string.h> // stderror
#include <errno.h>  // errno
#include <stdlib.h> // malloc, free
// network
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>   // close

#include "../common/network.h"
#include "../common/utils.h"

#include "net_mgr.h"

static int s_socketfd;
static struct sockaddr_in6 *s_serv_addr = NULL;
static const socklen_t s_slen = sizeof (struct sockaddr_in6);

int net_setup_socket (const char *host, const char *port)
{
	int err;
	struct addrinfo hints, *res = NULL;
	char address[INET6_ADDRSTRLEN];

	memset(&hints, 0, sizeof(hints));
	hints.ai_family   = AF_INET6;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;

	if ((err = getaddrinfo (host, port, &hints, &res)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror (err));
		if (err == EAI_SYSTEM)
			perror("getaddrinfo");
		return err;
	}
	if (res->ai_canonname)
		printf ("Connected: %s\n", res->ai_canonname);
	inet_ntop (AF_INET6, &(((struct sockaddr_in6 *)(res->ai_addr))->sin6_addr),
	           address, INET6_ADDRSTRLEN);
	printf ("IP address : %s\n", address);

	if ((s_socketfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1) {
		perror("socket");
		freeaddrinfo(res);
		return errno;
	}

	s_serv_addr = (struct sockaddr_in6*) malloc (sizeof (struct sockaddr_in6));
	if (s_serv_addr == NULL) {
		fprintf (stderr, "Failed to malloc: %s", strerror (errno));
		return errno;
	}
	memcpy (s_serv_addr, (struct sockaddr_in6*)res->ai_addr,
	        sizeof (struct sockaddr_in6)); // keep the sockadrr

	freeaddrinfo(res); // free the linked list

	return 0;
}

int net_close_socket (void)
{
	int err;

	free (s_serv_addr);

	err = close (s_socketfd);
	if (err == -1) {
		fprintf (stderr, "Failed to close the socket: %s\n", strerror (errno));
		return errno;
	}

	return 0;
}


/**
 * @brief at the init, we should receive a syn and an ack but the order can change
 * @param err      the error number of the previous call
 * @param type     type of the packet
 * @param window   the window
 * @param seq_num  the sequence number
 * @param xor_freq the frequency of XOR packages
 * @return 0 on success
 */
static int _get_syn_ack_init (int err, ptype_t type, uint8_t window,
                              uint8_t seq_num, uint8_t *xor_freq, uint8_t *init_window)
{
	if (err != 0)
		return err;

	if (type == PTYPE_SYN) {
		if (window < 2)
			*xor_freq = 0; // no XOR packet should be sent
		else
			*xor_freq = window;
		printf ("INIT: xor each %d time\n", *xor_freq);
	}
	else if (type == PTYPE_ACK) {
		LOG ("INIT: received first ACK");
		*init_window = window;
	}
	else {
		fprintf (stderr, "Received wrong type: %d", type);
		return -1;
	}

	// just to be sure
	if (seq_num != 0) {
		fprintf (stderr, "Received wrong SeqNum: %d instead of 0\n", seq_num);
		return -1;
	}

	return 0;
}

/*
The handshake is performed when both peers send a SYN packet to inform the
remote host of the requested XOR frequency. As they are the first packet sent by
both peers, their sequence number is 0.

Client             Server
  ----- syn(0) --->       SYN packet with win=0 (the sender will not receive data so it doesn't care about receiving XORs)
  <---- syn(0) ----       SYN packet with win=3 (the receiver wants XOR packet every 3 packets)
  <---- ack(0) ----       Receiver acknowledges the sender's SYN
  ----- ack(0) --->       Sender acknowledges the receiver's SYN
*/
int net_init_connexion (uint8_t *xor_freq, uint8_t *init_window)
{
	ptype_t type;
	uint8_t window, seq_num;
	struct sockaddr_in6 serv_addr;
	socklen_t slen;
	int err;

	// send first syn
	LOG ("INIT: send first SYN packet");
	err = net_send_header (s_socketfd, (const struct sockaddr*)s_serv_addr,
	                       s_slen, PTYPE_SYN, 0, 0);
	if (err != 0)
		return err;

	LOG ("INIT: recv first SYN/ACK packet (part 1)");
	// we should receive a SYN and an ACK
	err = net_get_header (s_socketfd, (struct sockaddr*)&serv_addr, &slen,
	                      &type, &window, &seq_num, NULL);
	if (_get_syn_ack_init (err, type, window, seq_num, xor_freq, init_window) != 0)
		return -1;

	LOG ("INIT: recv first SYN/ACK packet (part 2)");
	err = net_get_header (s_socketfd, (struct sockaddr*)&serv_addr, &slen,
	                      &type, &window, &seq_num, NULL);
	if (_get_syn_ack_init (err, type, window, seq_num, xor_freq, init_window) != 0)
		return -1;

	/// TODO: check serv_addr?

	LOG ("INIT: send first ACK packet");
	err = net_send_header (s_socketfd, (const struct sockaddr*)s_serv_addr,
	                       s_slen, PTYPE_ACK, 0, 0);
	if (err != 0)
		return err;

	return 0;
}

int net_send_packet (ptype_t type, uint8_t seqnum, char buf[PACKET_SIZE],
                     uint16_t length)
{
	ssize_t bytes;
	uint16_t len = htons (length);

	// prepare the buffer
	memset (buf, 0, PACKET_HEADER);
	buf[0] = type << 5;
	// buf[0] |= window & 0x1f;
	buf[1] = seqnum;
	memcpy (&buf[2], &len, sizeof (uint16_t));

	LOG ("Send Packet: send packet");
	bytes = sendto (s_socketfd, buf, PACKET_SIZE, 0,
	                (const struct sockaddr*)s_serv_addr, s_slen);
	if (bytes == -1) {
		fprintf (stderr, "Failed to send header: %s\n", strerror (errno));
		return errno;
	}
	if (bytes != PACKET_SIZE) {
		fprintf (stderr, "Failed to send all data of the header packet\n");
		return -1;
	}

	return 0;
}

int net_get_ack (ptype_t *type, uint8_t *window, uint8_t *seqnum)
{
	struct sockaddr_in6 serv_addr;
	socklen_t slen;
	return net_get_header(s_socketfd, (struct sockaddr*)&serv_addr, &slen, type,
	                      window, seqnum, NULL);
}

