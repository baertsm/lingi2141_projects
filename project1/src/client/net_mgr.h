#ifndef _NET_MGR_H_
#define _NET_MGR_H_

#include <stdint.h> // uint8_t
#include <sys/types.h> // socket
#include <sys/socket.h>
#include "../common/udp.h" // ptype_t

/**
 * @brief Initialize the socket
 * @param host that will be used
 * @param port that will be bind
 * @return 0 on success
 */
int net_setup_socket (const char *host, const char *port);

/**
 * @brief End and close the connexion
 * @return 0 on success
 */
int net_close_socket (void);

/**
 * @brief Initialise a connexion with a server
 * @param xor_freq the xor frequency [transfer]
 * @return 0 on success
 */
int net_init_connexion (uint8_t *xor_freq, uint8_t *init_window);

/**
 * @brief send a packet
 * @param type   type of the packet
 * @param seqnum the sequence number
 * @param buf    data to send are in buf[4+]
 * @param length length of the data to send
 * @return 0 on success or errno
 */
int net_send_packet (ptype_t type, uint8_t seqnum, char buf[PACKET_SIZE],
                     uint16_t length);

/**
 * @brief Get an ACK
 * @param type   type of the packet
 * @param window current window
 * @param seqnum the sequence number
 * @return 0 on success or errno
 */
int net_get_ack (ptype_t *type, uint8_t *window, uint8_t *seqnum);

#endif
