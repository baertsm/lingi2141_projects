#include <string.h> // stderror
#include <errno.h>  // errno
#include <arpa/inet.h> // ntohs

#include "utils.h"
#include "network.h"

int net_get_header (int sockfd, struct sockaddr *sockaddr, socklen_t *addrlen,
                    ptype_t *type, uint8_t *window, uint8_t *seqnum,
                    uint16_t *length)
{
	char buf[PACKET_HEADER];
	ssize_t bytes;
	uint16_t len;

	LOG ("Get Header: read packet");
	bytes = recvfrom (sockfd, buf, PACKET_HEADER, 0, sockaddr, addrlen);
	if (bytes == -1) {
		fprintf (stderr, "Failed to recv the header: %s\n", strerror (errno));
		return errno;
	}
	// let assume that we should receive this very small packet in one call
	if (bytes != PACKET_HEADER) {
		fprintf (stderr, "Failed to receive the all the header\n");
		return -1;
	}

	*type = (uint8_t) buf[0] >> 5;
	*window = buf[0] & 0x1f; // 0b00011111
	*seqnum = buf[1];

	if (length != NULL) {
		memcpy (&len, &buf[2], sizeof (char) * 2);
		*length = ntohs (len);
	}

	return 0;
}

int net_send_header (int sockfd, const struct sockaddr *sockaddr,
                     socklen_t addrlen, ptype_t type, uint8_t window,
                     uint8_t seqnum)
{
	char buf[PACKET_HEADER];
	ssize_t bytes;

	// prepare the buffer
	memset (buf, 0, PACKET_HEADER);
	buf[0] = type << 5;
	buf[0] |= window & 0x1f;
	buf[1] = seqnum;
	// all other data are 0;

	LOG ("Send Header: send header packet");
	bytes = sendto (sockfd, buf, PACKET_HEADER, 0, sockaddr, addrlen);
	if (bytes == -1) {
		fprintf (stderr, "Failed to send header: %s\n", strerror (errno));
		return errno;
	}
	if (bytes != PACKET_HEADER) {
		fprintf (stderr, "Failed to send all data of the header packet\n");
		return -1;
	}

	return 0;
}
