#ifndef _NETWORK_H_
#define _NETWORK_H_

#include <stdint.h> // uint8_t
#include <sys/types.h> // socket
#include <sys/socket.h>
#include "udp.h" // ptype_t

/**
 * @brief get info from the header
 * @param sockfd   the file descriptor of the socket
 * @param sockaddr sockaddr structure that will be filled in
 * @param addrlen  size of the sockaddr struct that will be filled in
 * @param type     type of the packet
 * @param window   current window
 * @param seqnum   the sequence number
 * @param length   the length of the payload (optional: set to NULL)
 * @return 0 on success or errno
 */
int net_get_header (int sockfd, struct sockaddr *sockaddr, socklen_t *addrlen,
                    ptype_t *type, uint8_t *window, uint8_t *seqnum,
                    uint16_t *length);


/**
 * @brief send an header
 * @param sockfd   the file descriptor of the socket
 * @param sockaddr the address of the destination
 * @param addrlen  the size of sockaddr
 * @param type     type of the packet
 * @param window   the window (max 2^5 - 1)
 * @param seqnum   the sequence number
 * @return 0 on success or errno
 */
int net_send_header (int sockfd, const struct sockaddr *sockaddr,
                     socklen_t addrlen, ptype_t type, uint8_t window,
                     uint8_t seqnum);

#endif
