#include <stdint.h> // uint8_t
#include <stdlib.h> // free, malloc
#include <stdio.h>  // fprintfq
#include <string.h> // strerror
#include <errno.h>  // errno

#include "packet.h"

packet_t ** packets_init (int size)
{
	int i, err;

	packet_t **packets = (packet_t **) malloc (sizeof (packet_t *) * size);
	if (packets == NULL) {
		fprintf (stderr, "Failed to malloc: %s\n", strerror (errno));
		return NULL;
	}
	for (i = 0 ; i < size ; i++) {
		packets[i] = (packet_t *) malloc (sizeof (packet_t));
		if (packets[i] == NULL) {
			fprintf (stderr, "Failed to malloc: %s\n", strerror (errno));
			break;
		}

		packets[i]->data = (char *) malloc (sizeof (char) * PACKET_SIZE);
		if (packets[i]->data == NULL) {
			fprintf (stderr, "Failed to malloc: %s\n", strerror (errno));
			free (packets[i]);
			break;
		}
	}

	// problem with a malloc?
	if (i < size) {
		err = i;
		for (i = 0 ; i < err ; i++) {
			free (packets[i]->data);
			free (packets[i]);
		}
		free (packets);
		packets = NULL;
	}

	return packets;
}

void packets_free (packet_t **packets, int size)
{
	int i;

	if (packets != NULL) {
		for (i = 0 ; i < size ; i++) {
			free (packets[i]->data);
			free (packets[i]);
		}
		free (packets);
	}
}
