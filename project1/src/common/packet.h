#ifndef _PACKET_H
#define _PACKET_H

#include <stdint.h> // uint8_t
#include "udp.h"

typedef enum {
	EMPTY = 0,       // no data
	PACKET_SENT,     // sent, wait for the ACK
	PACKET_RECEIVED, // packet received, wait in the buffer until XOR is received and is correct
	ACK_RECEIVED     // ACK received, maybe we need to wait for the next XOR
} state_t;

typedef struct {
    ptype_t  type;
    uint8_t  seq_num;
    char    *data;
    uint16_t len;
    state_t  state;
} packet_t;

/**
 * @brief Initialise an array of packet_t
 * @param size number of item in this array
 * @return an array of packet_t
 */
packet_t ** packets_init (int size);

/**
 * @brief Free an array of packet_t
 * @param packets array to free
 * @param size number of item in this array
 */
void packets_free (packet_t **packets, int size);

#endif
