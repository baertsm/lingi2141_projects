
#include "queue.h"
#include <stdlib.h>
#include <stdio.h>

#define _is_empty(pQueue) pQueue->first == NULL

queue_t *queue_init (void)
{
	queue_t *pQueue = (queue_t *) malloc (sizeof (queue_t));
	if (pQueue == NULL)
	{
		fprintf (stderr, "Not able to init the queue: no more space?");
		exit (EXIT_FAILURE);
	}

	pQueue->first = NULL;
	pQueue->last = NULL;

	return pQueue;
}

void enqueue (queue_t *q, void *data)
{
	if (q == NULL)
		return;

	element_queue *pElem = (element_queue *) malloc (sizeof (element_queue));
	if (pElem == NULL)
	{
		fprintf (stderr, "Not able to init the element: no more space?");
		exit (EXIT_FAILURE);
	}
	pElem->next = NULL;
	pElem->data = data;

	if (_is_empty (q))
		q->first = pElem;
	else
		q->last->next = pElem;

	q->last = pElem;
}

void *dequeue (queue_t *q)
{
	if (q == NULL || _is_empty (q))
		return NULL;

	void *data = q->first->data; // we need data (we have to return it)
	element_queue *next = q->first->next; // we need the next element => the first
	free (q->first);	// we can now free the first element
	q->first = next;	// the first element is now the former second element. (can be NULL => empty queue)

	return data;
}

void queue_free (queue_t *q)
{
	if (q == NULL)
		return;

	free (q);
}
