#ifndef __QUEUE_H__
#define __QUEUE_H__
 
typedef struct Element{
    struct Element *next;
    void *data;
} element_queue;

struct Queue {
	element_queue *first;
	element_queue *last;
};

typedef struct Queue queue_t;

/**
 * Init
 */
queue_t *queue_init (void);

/**
 * Add a new element at the end of the queue with the data in the queue q (that has to be initialised...)
 * (except that here there is an error message is it's not the case)
 */
void enqueue (queue_t *q, void *data);

/**
 * Remove and return the first element (should not be empty but an error message is used here)
 */
void *dequeue (queue_t *q);

/**
 * Free an empty queue (should be empty and not null...)
 */
void queue_free (queue_t *q);

/**
 * Free a queue (should be empty and not null...)
 */
void queue_free (queue_t *q);

#endif
