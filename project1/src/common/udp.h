#ifndef _UPD_H_
#define _UPD_H_

/*
 0 2 3   7 8     15 16    23 24    31
+---+-----+--------+--------+--------+
|Ty | Win |  Seq   |     Length      |
|pe | dow |  num   |                 |
+---+-----+--------+--------+--------+

* Type:    3 bits
* Window:  5 bits
* SeqNum:  8 bits
* Length: 16 bits
*/

#define PACKET_HEADER 4
#define PACKET_PAYLOAD 512
#define PACKET_SIZE (PACKET_HEADER + PACKET_PAYLOAD)

#define SEQNUM_MAX_SIZE 240

/* Type: 4 types */
typedef enum {
    PTYPE_DATA = 0x1, // header + payload
    PTYPE_ACK  = 0x2, // header
    PTYPE_SYN  = 0x3, // header
    PTYPE_XOR  = 0x4  // header + payload
} ptype_t;

#endif
