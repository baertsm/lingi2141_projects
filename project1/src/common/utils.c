/*
 author: jbenet
 https://gist.github.com/jbenet/1087739
 
 */


#include "utils.h"

void current_utc_time(struct timespec *ts) {
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
	clock_serv_t cclock;
	mach_timespec_t mts;
	host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
	clock_get_time(cclock, &mts);
	mach_port_deallocate(mach_task_self(), cclock);
	ts->tv_sec = mts.tv_sec;
	ts->tv_nsec = mts.tv_nsec;
#else
	clock_gettime(CLOCK_REALTIME, ts);
#endif
}

int utils_get_index (int max, int i)
{
	if (i < 0)
		return max + i; // e.g. i = -1, max = 10 => 9
	return i;
}
