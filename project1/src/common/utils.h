#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h> // printf

#include <time.h>
#include <sys/time.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

// #define DEBUG // force debug

#define UNUSED(x) (void)(x)

#ifndef DEBUG
#define LOG(x)
#else
#define LOG(x) printf("%s:%d\t%s\n", __FILE__, __LINE__, x)
#endif

void current_utc_time(struct timespec *ts);

/**
 * @brief get the index of an element which can be out of the limit
 * @param max the maximum element
 * @param i the current index (can be < 0 or > maw)
 * @return the index that can be used in a tab
 */
int utils_get_index (int max, int i);

#endif
