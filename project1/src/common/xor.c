#include <arpa/inet.h> // htons
#include <string.h>    // memset, memcpy

#include "utils.h"
#include "xor.h"

/**
 * @brief Create a XOR package by using 2 packages: only modify the payload
 * @param buf1    the first packet
 * @param buf2    the second packet
 * @param new_buf the new packet which will have a payload which is a XOR of the
 *                two other buffers
 */
static void _xor_two_packets (packet_t *p1, packet_t *p2, packet_t *new_pack)
{
	int i;

	for (i = PACKET_HEADER ; i < PACKET_SIZE ; i++)
		new_pack->data[i] = p1->data[i] ^ p2->data[i];
}

void xor_create_packet (packet_t **packets, packet_t *packet, uint8_t xor_freq,
                        int n, int size, uint8_t seq_num)
{
	int i, i2;
	packet_t *packet1 = packets[n];

	packet->type = PTYPE_XOR;
	packet->seq_num = seq_num;
	packet->len = PACKET_PAYLOAD;

	for (i = 1 ; i < xor_freq - 1 ; i++) {
		if (i == 1) // first: we can XOR 2 packets and fill in the new one
			packet1 = packets[utils_get_index (size, n - i)];
		else // else, we have to continue the XOR and use the old result
			packet1 = packet;
		i2 = utils_get_index (size, n - i - 1);

		_xor_two_packets (packet1, packets[i2], packet);
	}
}

int xor_compare_packets (packet_t *packet1, packet_t *packet2)
{
	int i;
	char *buf1 = packet1->data;
	char *buf2 = packet2->data;

	for (i = PACKET_HEADER ; i < PACKET_SIZE ; i++) {
		if (buf1[i] != buf2[i])
			return -1;
	}

	return 0;
}
