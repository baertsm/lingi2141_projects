#ifndef _XOR_H_
#define _XOR_H_

#include <stdint.h> // uint8_t
#include "udp.h" // PACKET_SIZE
#include "packet.h" // packet_t

/**
 * @brief create the XOR packet (s_packets[n])
 * @param packets  a tab of packets
 * @param packet   the current packet to fill in
 * @param xor_freq the frequency of the XOR
 * @param n        the index of the current packet to fill in
 * @param size     the number of packets in 'packets'
 * @param seq_num  the sequence number
 */
void xor_create_packet (packet_t **packets, packet_t *packet, uint8_t xor_freq,
                        int n, int size, uint8_t seq_num);

/**
 * @brief Compare two packages to check if their payload are the same
 * @param packet1 the first packet
 * @param packet2 the second packet
 * @return 0 if the packages are the same
 */
int xor_compare_packets (packet_t *packet1, packet_t *packet2);

#endif
