#include <stdio.h> // getchar, fprintf
#include <stdlib.h> // getchar
#include <string.h> // strerror
#include <errno.h>  // errno

#include "../common/packet.h"
#include "../common/utils.h"
#include "../common/xor.h"

#include "file_mgr.h"
#include "net_mgr.h"
#include "connexion.h"

#define BUFF_ELEM 10 // SEQNUM_MAX_SIZE % BUFF_ELEM == 0

static uint8_t s_window = BUFF_ELEM;
static packet_t **s_packets = NULL;
static uint16_t s_seqnum = 0;

int connexion_init (void)
{
	s_packets = packets_init (BUFF_ELEM);
	if (s_packets == NULL)
		return -1;

	return 0;
}

/**
 * @brief save packets to file
 * @param n          current packet
 * @param nb_packets number of packets before n to save
 * @return 0 on success
 */
static int _save_packets_to_file (int n, int nb_packets)
{
	int i, elem;
	char *buf;

	for (i = nb_packets ; i > 0 ; i--) {
		elem = utils_get_index (BUFF_ELEM, n - i);
		buf = s_packets[elem]->data;
		if (save_file (buf + PACKET_HEADER, s_packets[elem]->len) != 0)
			return -1;
		s_packets[elem]->state = EMPTY;
		s_window++;
	}

	return 0;
}

/**
 * @brief get the file sent by this client
 * @return 0 on success
 */
static int _get_file (void)
{
	ptype_t  type;
	uint8_t  seqnum;
	uint16_t length;
	packet_t *packet, *packet_xor;
	int n, err = 0;
	char *buf;

	buf = (char *)malloc (sizeof (char) * PACKET_SIZE);
	if (buf == NULL){
		fprintf (stderr, "Failed to malloc: %s\n", strerror (errno));
		return errno;
	}

	packet_xor = (packet_t *)malloc (sizeof (packet_t));
	if (packet_xor == NULL){
		fprintf (stderr, "Failed to malloc: %s\n", strerror (errno));
		free (buf);
		return errno;
	}
	packet_xor->data = buf;

	while (1) {
		LOG ("start getting chunk");
		err = net_get_data (&type, &seqnum, &length, buf);
		if (err != 0)
			break;

		if (s_window <= 0 && seqnum > packet->seq_num) { // window full and new seq
			fprintf (stderr, "Window is full! Drop packages\n");
			continue;
		}

		// check if we receive a packet with the next seqnum or a retransmission
		if (type == PTYPE_DATA && (s_seqnum + 1) % SEQNUM_MAX_SIZE < seqnum) { // packages are missing
			err = net_send_ack (s_window, s_seqnum); // ACK with the previous seqnum: we need a retransmission
			if (err != 0)
				break;
			continue;
		}

		s_seqnum = seqnum;
		n = (seqnum % BUFF_ELEM);

		packet = s_packets[n];
		packet->type = type;
		packet->seq_num = seqnum;
		packet->len = length;
		packet->state = PACKET_RECEIVED;
		memcpy (packet->data, buf, PACKET_SIZE);
		s_window--;

		if (type == PTYPE_DATA) {
			err = net_send_ack (s_window, seqnum);
			if (err != 0)
				break;
			if (length == 0 && XOR_FREQ > 1) { // end of the file: save previous chunks if any
				err = _save_packets_to_file (n % BUFF_ELEM, utils_get_index (SEQNUM_MAX_SIZE, seqnum - 1) % (XOR_FREQ + 1));
				break;
			}
		}
		else if (XOR_FREQ > 1) { // PTYPE_XOR: check other XOR
			xor_create_packet (s_packets, packet_xor, XOR_FREQ + 1, n,
			                   BUFF_ELEM, 0);
			if (xor_compare_packets (packet, packet_xor) != 0) {
				// problem: ACK with the previous seqnum: we need to receive the previous packages to recreate the XOR
				err =  net_send_ack (s_window, utils_get_index (SEQNUM_MAX_SIZE,
				                     seqnum - XOR_FREQ - 1));
				if (err != 0)
					break;
			}
			else {
				err = net_send_ack (s_window, seqnum);
				if (err != 0)
					break;
				err = _save_packets_to_file (n, XOR_FREQ);
				if (err != 0)
					break;
				packet->state = EMPTY;
				s_window++; // free this packet
			}
		}

		if (XOR_FREQ <= 1) {
			err = _save_packets_to_file ((n + 1) % BUFF_ELEM, 1);
			if (err != 0)
				break;
		}
	}

	free (buf);
	free (packet_xor);

	return err;
}

int connexion_launch_daemon (char *filename)
{
	
	while (1) {
		if (open_file (filename) != 0)
			return EXIT_FAILURE;

		if (net_init_connexion (BUFF_ELEM) != 0) {
			delete_file (filename);
			continue;
		}

		s_window = BUFF_ELEM;
		s_seqnum = 0;

		printf ("The file (%s) is ready, waiting for a client\n", filename);

		// window It states the receiving window of the sender of the segment in DATA, ACK and XOR packets.
		if (_get_file () != 0)
			delete_file (filename);
		else
			close_file ();

		LOG ("Get all the file");

		printf ("Press Enter to continue\n");
		getchar ();
	}

	return 0;
}

int connexion_end (int bCloseFile)
{
	int err;

	err = net_close_socket ();
	if (bCloseFile)
		err |= close_file ();

	packets_free (s_packets, BUFF_ELEM);
	s_packets = NULL;

	return err;
}
