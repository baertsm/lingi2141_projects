#ifndef _CONNEXION_H_
#define _CONNEXION_H_

/**
 * @brief launch the daemon to accept connexion and transfer file
 * @param filename path of the file to save data
 * @return the exit status
 */
int connexion_launch_daemon (char *filename);

/**
 * @brief init and allocate resources
 * @return 0 on success or errno
 */
int connexion_init (void);

/**
 * @brief close the connexion, the file and free allocated resources
 * @param bCloseFile true to close the file (e.g. when stopping a transfer)
 * @return 0 on success or errno
 */
int connexion_end (int bCloseFile);

#endif
