#include <string.h> // stderror
#include <errno.h>  // errno
#include <sys/stat.h> // open
#include <fcntl.h>    // open
#include <unistd.h>   // close, unlink

#include "../common/utils.h"

#include "file_mgr.h"

static int s_filefd = -1;

int open_file (const char *filename)
{
	s_filefd = open (filename, O_WRONLY | O_CREAT | O_TRUNC,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH); /* 664 */
	if (s_filefd < 0) {
		fprintf (stderr, "Failed to open the file to write: %s\n",
				 strerror (errno));
		return errno;
	}
	return 0;
}

int save_file (const char *buf, uint16_t length)
{
	int err;

	err = write(s_filefd, buf, length);
	if (err < 0) {
		fprintf (stderr, "Failed to write the file: %s\n",
				 strerror (errno));
		return errno;
	}

	return 0;
}

int close_file (void)
{
	int err;

	// not using a file
	if (s_filefd < 0)
		return -1;

	err = close (s_filefd);
	s_filefd = -1;
	if (err == -1) {
		fprintf (stderr, "Failed to close the file: %s\n", strerror (errno));
		return errno;
	}

	return 0;
}

int delete_file (const char *filename)
{
	int err;

	err = close_file ();
	if (err != 0)
		return err;

	if (unlink (filename) == -1)
		return errno;

	return 0;
}
