#ifndef _FILE_MGR_H_
#define _FILE_MGR_H_

#include <stdint.h> // uint16_t

/**
 * @brief Open the file with write rights (664)
 * @param filename the name of the file
 * @return 0 on success or errno
 */
int open_file (const char *filename);

/**
 * @brief save data to the current file
 * @param buf    the buffer to export to a file
 * @param length the length of this buffer
 * @return 0 on success or errno
 */
int save_file (const char *buf, uint16_t length);

/**
 * @brief Close the opened file
 * @return 0 on success or errno
 */
int close_file (void);

/**
 * @brief Delete the file
 * @param filename file to delete
 * @return 0 on success or errno
 */
int delete_file (const char *filename);

#endif
