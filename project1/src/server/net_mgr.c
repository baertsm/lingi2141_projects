#include <string.h> // stderror
#include <errno.h>  // errno
// network
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>   // close

#include "../common/network.h"
#include "../common/utils.h"

#include "net_mgr.h"

static int s_socketfd;
static struct sockaddr_in6 s_client_addr;
static socklen_t s_slen = sizeof (s_client_addr);

int net_setup_socket (int port)
{
	struct sockaddr_in6 address;

	// Init the socket:
	s_socketfd = socket(AF_INET6, SOCK_DGRAM, 0);
	if (s_socketfd == -1)
	{
		fprintf (stderr, "Couldn't create the listening socket: %s\n",
		         strerror (errno));
		return errno;
	}

	// Bind: IPv6 on port SERVER_PORT
	memset (&address, 0, sizeof (address));
	address.sin6_family   = AF_INET6;
	address.sin6_addr     = in6addr_any;
	address.sin6_port     = htons(port);
	if (bind (s_socketfd, (struct sockaddr *)&address , sizeof (address)) == -1) {
		fprintf (stderr, "Failed to bind: %s\n", strerror (errno));
		return -1;
	}
	return 0;
}

int net_close_socket (void)
{
	int err;

	err = close (s_socketfd);
	if (err == -1) {
		fprintf (stderr, "Failed to close the socket: %s\n", strerror (errno));
		return errno;
	}

	return 0;
}

/**
 * @brief just drop the payload to not be desynchronised
 */
static void _drop_payload (void)
{
	char buf[PACKET_PAYLOAD];
	// No need to check error
	recvfrom (s_socketfd, buf, PACKET_PAYLOAD, 0, NULL, NULL);
}

/*
The handshake is performed when both peers send a SYN packet to inform the
remote host of the requested XOR frequency. As they are the first packet sent by
both peers, their sequence number is 0.

Client             Server
  ----- syn(0) --->       SYN packet with win=0 (the sender will not receive data so it doesn't care about receiving XORs)
  <---- syn(0) ----       SYN packet with win=3 (the receiver wants XOR packet every 3 packets)
  <---- ack(0) ----       Receiver acknowledges the sender's SYN
  ----- ack(0) --->       Sender acknowledges the receiver's SYN
*/
int net_init_connexion (uint8_t init_window)
{
	ptype_t type;
	uint8_t window, seq_num;
	struct sockaddr_in6 client_addr;
	socklen_t slen = sizeof (client_addr);
	int err;

	LOG ("INIT: recv first ACK packet");
	err = net_get_header (s_socketfd, (struct sockaddr*)&s_client_addr, &s_slen,
	                      &type, &window, &seq_num, NULL);
	if (err != 0)
		return err;
	if (type != PTYPE_SYN) {
		fprintf (stderr, "Received wrong type: %d instead of %d\n", type, PTYPE_SYN);
		if (type != PTYPE_ACK) // read payload to not be desynchronised
			_drop_payload ();
		return -1;
	}
	if (window != 0) {
		fprintf (stderr, "Received wrong window: %d instead of 0\n", window);
		return -1;
	}
	if (seq_num != 0) {
		fprintf (stderr, "Received wrong SeqNum: %d instead of 0\n", seq_num);
		return -1;
	}

	// send syn XOR_FREQ
	LOG ("INIT: send first SYN packet");
	err = net_send_header (s_socketfd, (const struct sockaddr*)&s_client_addr,
	                       s_slen, PTYPE_SYN, XOR_FREQ, 0);
	if (err != 0)
		return err;

	// send ACK
	LOG ("INIT: send first ACK packet");
	err = net_send_header (s_socketfd, (const struct sockaddr*)&s_client_addr,
	                       s_slen, PTYPE_ACK, init_window, 0);
	if (err != 0)
		return err;

	// read first ACK to end the initialisation
	LOG ("INIT: read first ACK");
	err = net_get_header (s_socketfd, (struct sockaddr*)&client_addr, &slen,
	                      &type, &window, &seq_num, NULL);
	if (err != 0)
		return err;
	if (type != PTYPE_ACK) {
		fprintf (stderr, "Received wrong type: %d instead of %d\n", type, PTYPE_ACK);
		if (type != PTYPE_SYN) // read payload to not be desynchronised
			_drop_payload ();
		return -1;
	}
	if (window != 0) {
		fprintf (stderr, "Received wrong window: %d instead of 0\n", window);
		return -1;
	}
	if (seq_num != 0) {
		fprintf (stderr, "Received wrong SeqNum: %d instead of 0\n", seq_num);
		return -1;
	}
	/// TODO? check if the client is the same: c.sin6_port + c.sin6_addr.s6_addr

	return 0;
}

int net_get_data (ptype_t *type, uint8_t *seqnum, uint16_t *length,
                  char buf[PACKET_SIZE])
{
	struct sockaddr_in6 sockaddr;
	socklen_t addrlen = sizeof (sockaddr);
	ssize_t bytes;
	uint16_t len;

	bytes = recvfrom (s_socketfd, buf, PACKET_SIZE, 0,
	                  (struct sockaddr *)&sockaddr, &addrlen);
	if (bytes == -1) {
		fprintf (stderr, "Failed to recv the packet: %s\n", strerror (errno));
		return errno;
	}
	// let assume that we should receive this very small packet in one call
	if (bytes != PACKET_SIZE) {
		fprintf (stderr, "Failed to receive the all the packet\n");
		return -1;
	}

	*type = (uint8_t) buf[0] >> 5;
	// *window = buf[0] & 0x1f; // 0b00011111
	*seqnum = buf[1];
	memcpy (&len, &buf[2], sizeof (char) * 2);
	*length = ntohs (len);

	if (*length > PACKET_PAYLOAD) {
		fprintf (stderr, "Length is too big: %d\n", *length);
		return -1;
	}

	if (*type != PTYPE_DATA && *type != PTYPE_XOR) {
		fprintf (stderr, "Received wrong type: %d\n", *type);
		return -1;
	}

	return 0;
}


int net_send_ack (uint8_t window, uint8_t seq_num)
{
	int err;
	err = net_send_header (s_socketfd, (const struct sockaddr*)&s_client_addr,
	                       s_slen, PTYPE_ACK, window, seq_num);
	if (err != 0)
		return err;

	return 0;
}
