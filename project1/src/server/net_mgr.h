#ifndef _NET_MGR_H_
#define _NET_MGR_H_

#include <stdint.h> // uint8_t
#include <sys/types.h> // socket
#include <sys/socket.h>
#include "../common/udp.h" // ptype_t

#define XOR_FREQ 0x2 // SEQNUM_MAX_SIZE % (XOR_FREQ + 1) == 0

/**
 * @brief Initialise and bind the listening socket
 * @param port that will be bind
 * @return 0 on success
 */
int net_setup_socket (int port);

/**
 * @brief End and close the connexion
 * @return 0 on success
 */
int net_close_socket (void);

/**
 * @brief Initialise one connexion with a client
 * @param window initial size of the window
 * @return 0 on success
 */
int net_init_connexion (uint8_t init_window);

/**
 * @brief get data from the packet
 * @param type     the type of the packet
 * @param seqnum   the sequence number
 * @param length   the length of the payload
 * @param buf      the buffer to fill in
 * @return 0 on success or errno
 */
int net_get_data (ptype_t *type, uint8_t *seqnum, uint16_t *length,
                  char buf[PACKET_SIZE]);

/**
 * @brief send an ACK to the client
 * @param window  the current window
 * @param seq_num the sequence number to ACK
 * @return 0 on success or errno
 */
int net_send_ack (uint8_t window, uint8_t seq_num);

#endif
