#include <stdlib.h>   // exit, atoi, etc.
#include <signal.h>   // signal

#include "../common/udp.h"
#include "../common/utils.h"
#include "../common/network.h"
#include "file_mgr.h"
#include "net_mgr.h"
#include "connexion.h"

static void _sig_handler (int sig_number)
{
	connexion_end (1);

	printf ("Received signal (%d): exit\n", sig_number);
	exit (EXIT_SUCCESS);
}
/**
 * @brief capture_signals will set the process sigmask
 */
static void _capture_signals (void)
{
	signal (SIGINT,  _sig_handler);
	signal (SIGQUIT, _sig_handler);
	signal (SIGTERM, _sig_handler);
}

/**
 * ./server.bin port filename
 */
int main (int argc, char **argv)
{
	int port, err;
	char* filename;

	if (argc < 3) {
		fprintf (stderr, "Usage: %s <port> <filepath>\n", argv[0]);
		return EXIT_FAILURE;
	}
	port = atoi (argv[1]);
	filename = argv[2];

	if (net_setup_socket (port) != 0)
		return EXIT_FAILURE;

	if (connexion_init () != 0) {
		net_close_socket ();
		return EXIT_FAILURE;
	}

	_capture_signals ();

	printf ("Waiting for a client on port %d\n", port);


	// run daemon
	err = connexion_launch_daemon (filename);

	connexion_end (0);

	return err;
}

